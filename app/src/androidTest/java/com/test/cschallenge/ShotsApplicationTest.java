package com.test.cschallenge;

import com.test.cschallenge.ui.ShotsActivity;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.espresso.contrib.RecyclerViewActions;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class ShotsApplicationTest{

    private int position = 0;

    @Rule
    public ActivityTestRule<ShotsActivity> mShotsActivityRule = new ActivityTestRule<>(ShotsActivity.class);

    @Test
    public void testOpenDetails(){
        onView(withId(R.id.shots_recycler_view))
            .perform(RecyclerViewActions.actionOnItemAtPosition(this.position, click()));
        onView(allOf(withId(R.id.author_username_textview), withParent(withId(R.id.author_info_container))))
            .check(matches(withText(containsString("By"))));
    }

    @Test
    public void testOpenDetailsAndBack(){
        testOpenDetails();
        onView(withId(R.id.detail_back_btn)).perform(click());
        onView(withId(R.id.shots_recycler_view)).perform(RecyclerViewActions.scrollToPosition(7));
        onView(withId(R.id.shots_recycler_view)).perform(RecyclerViewActions.scrollToPosition(0));
        onView(withId(R.id.detail_back_btn)).check(doesNotExist());
    }

    @Test
    public void testMultipleShots(){
        for(int i = 1; i <= 4; i++){
            position = i;
            testOpenDetailsAndBack();
        }
    }
}
